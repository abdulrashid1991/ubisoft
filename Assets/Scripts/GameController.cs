﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    Menu, Iniating, Ready, End, Pause, Play,
}

public class GameConfig
{
    public int _Row = 5, _Column = 2;
    public float DefaultEnemySpeed = 0.5f;
    public int LevelSpeedIncrFactor = 2;
    public int PlayerLife = 4;
    public int ScoreForEachHit = 10;
    public float _MinShootDelay = 1f, _MaxShhotDelay = 3f;

    public static GameConfig GetGameConfig()
    {
        return new GameConfig();
    }
}

public class PlayerStats
{
    public int Level;
    public int LifeLeft;
    public int Score;
}

public class GameController : MonoBehaviour
{
    public static GameController Instance;

    private static GameConfig mGameConfig;
    private static GameConfig GameConfig
    {
        get
        {
            if (mGameConfig == null)
            {
                mGameConfig = GameConfig.GetGameConfig();
            }
            return mGameConfig;
        }
    }

    [SerializeField]private EnemyHandler _EnemyHandler;
    [SerializeField]private UIHandler _UiHandler;
    public GameObject _BombExplotionPrefab;
    public GameObject _BuletExplotionPrefab;
    public GameState mCurrGameSatate;
    public PlayerStats mCurrPlayerStats;

    private float mEnemyShootMinSpeed, mEnemyShootMaxSpeed;
    private float mEnemyMoveSpeed;
    private int mEnemyRowCount;
    private int mScoreIncrFactor;

    private int Level
    {
        get
        {
            return mCurrPlayerStats.Level;
        }
        set
        {
            mCurrPlayerStats.Level = value;
            _UiHandler.SetLevel(mCurrPlayerStats.Level);
        }

    }
    private int Score
    {
        get
        {
            return mCurrPlayerStats.Score;
        }
        set
        {
            mCurrPlayerStats.Score = value;
            _UiHandler.SetScore(mCurrPlayerStats.Score);
        }
    }
    private int Life
    {
        get
        {
            return mCurrPlayerStats.LifeLeft;
        }
        set
        {
            mCurrPlayerStats.LifeLeft = value;
            _UiHandler.SetLife(mCurrPlayerStats.LifeLeft);
        }
    }

    private void Awake()
    {
        Instance = this;
        GameObject.DontDestroyOnLoad(this.gameObject);
    }

    public void OnPlayerHit()
    {
        if (Life > 1)
        {
            Life -= 1;
        }
        else
        {
            Life = 0;
            mCurrGameSatate = GameState.End;
        }
    }

    public void OnBotHit()
    {
        Score += mScoreIncrFactor;
    }

    public void OnClickStartGame()
    {
        mEnemyMoveSpeed = GameConfig.DefaultEnemySpeed;
        mEnemyRowCount = GameConfig._Row;
        mScoreIncrFactor = GameConfig.ScoreForEachHit;
        mEnemyShootMinSpeed = GameConfig._MinShootDelay;
        mEnemyShootMaxSpeed = GameConfig._MaxShhotDelay;
        mCurrPlayerStats = new PlayerStats() { };
        Life = GameConfig.PlayerLife;
        Level = Score = 0;
        _EnemyHandler.InitEnemyCloud(mEnemyRowCount, GameConfig._Column, mEnemyMoveSpeed, mEnemyShootMinSpeed, mEnemyShootMaxSpeed);
    }

    public void IncreaseLevel()
    {
        Level += 1;
        mEnemyMoveSpeed *= GameConfig.LevelSpeedIncrFactor;
        mScoreIncrFactor *= GameConfig.LevelSpeedIncrFactor;
        mEnemyShootMinSpeed -= 0.25f;
        mEnemyShootMaxSpeed -= 0.5f;
        mEnemyRowCount += 1;
        _EnemyHandler.InitEnemyCloud(mEnemyRowCount, GameConfig._Column, mEnemyMoveSpeed, mEnemyShootMinSpeed, mEnemyShootMaxSpeed);
    }
}
