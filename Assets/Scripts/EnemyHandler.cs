﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class EnemyHandler : MonoBehaviour
{
    public GameObject _BotPref;
    public Transform _SpawnPoint;
    public float _RowIndent, _ColIndent;
    public List<List<Bot>> _CloudRef;
    public float _SlideSpeed;
    public AudioSource _Audio;

    private float mMoveSpeed;
    private float mWaitSeconds;
    private int mRow;
    private int mRandRow;
    private int mDirecion = 1;
    private bool mIsDirSwitching;
    private float mMinShootDelay = 1f, mMaxShhotDelay = 3f;

    public void InitEnemyCloud(int row,int coloumn,float moveSpeed, float minShootDelay, float maxShootDelay)
    {
        this.StopAllCoroutines();
        transform.position = _SpawnPoint.position;
        mRow = row;
        mMoveSpeed = moveSpeed;
        mMinShootDelay = minShootDelay;
        mMaxShhotDelay = maxShootDelay;
        this.StartCoroutine(InitCloud(row, coloumn));
    } 

    private void Update()
    {
        if (GameController.Instance.mCurrGameSatate == GameState.Ready)
        {
            transform.Translate(_SlideSpeed * Time.deltaTime * mDirecion,0,-mMoveSpeed * Time.deltaTime);
        }
    }
    
    public IEnumerator InitCloud(int row, int col)
    {
        GameController.Instance.mCurrGameSatate = GameState.Iniating;
        _Audio.Play();
        Vector3 initPos = Vector3.zero;
        List<Bot> bots = new List<Bot>();
        ClearAndInitBots();
        for (int rowID = 0; rowID < row; rowID++)
        {
            bots = new List<Bot>();
            for (int colId = 0; colId < col; colId++)
            {
                initPos = transform.position - new Vector3(_RowIndent * rowID, 0, _ColIndent * colId);
                Bot bot = ((GameObject)GameObject.Instantiate(_BotPref, initPos, Quaternion.identity)).GetComponent<Bot>();
                bot.transform.parent = this.transform;
                bot.mEnemyGandlerRef = this;
                bots.Add(bot);
                yield return new WaitForEndOfFrame();
            }
            _CloudRef.Add(bots);
        }
        GameController.Instance.mCurrGameSatate = GameState.Ready;
        StartCoroutine(ShootPlayer());
        yield break;
    }

    public void OnBotDestroy()
    {
        StartCoroutine("OnBotDestroyed");
    }

    private IEnumerator OnBotDestroyed()
    {
        GameController.Instance.OnBotHit();
        yield return new WaitForEndOfFrame();
        if (!(_CloudRef.Exists((botroe) => botroe.Exists((item) => item != null))))
        {
            GameController.Instance.IncreaseLevel();
        }
    }

    private IEnumerator ShootPlayer()
    {
        while (GameController.Instance.mCurrGameSatate == GameState.Ready)
        {
            yield return new WaitForSeconds(mWaitSeconds);
            mRandRow = Random.Range(0, mRow - 1);
            if (_CloudRef[mRandRow].Exists((bot) => bot != null) && _CloudRef[mRandRow].Last() == null)
            {
                _CloudRef[mRandRow].Last((bot) => bot != null).Shoot();
                mWaitSeconds = Random.Range(mMinShootDelay, mMaxShhotDelay);
            }
            else
            {
                mWaitSeconds = 0;
            }
        }
        yield break;
    }

    private void ClearAndInitBots()
    {
        if (_CloudRef != null)
        {
            _CloudRef.All((rowitems) =>
            {
                rowitems.All((bot) =>
                {
                    if (bot != null)
                    {
                        bot.Kill();
                    }
                    return true;
                });
                return true;
            });
            _CloudRef.Clear();
        }
        else
        {
            _CloudRef = new List<List<Bot>>();
        }
    }
    
    public void UpdateDirection()
    {
        if (!mIsDirSwitching)
        {
            this.StartCoroutine(UpdateDir());
        }
    }

    private IEnumerator UpdateDir()
    {
        mIsDirSwitching = true;
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        mDirecion *= -1;
        mIsDirSwitching = false;
    }

    
}
