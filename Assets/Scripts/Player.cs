﻿using UnityEngine;
using System.Collections;
using System;

public interface IOnHittable
{
    void OnObjectHit();
}

public class Player : MonoBehaviour, IOnHittable
{
    public float _Speed;
    public GameObject _BulletPref;
    public GameObject _BulletNuzzle;

    private Bullet mBullet;
    private float mInput;
    private bool mFreezeMoveLeft, mFreezeMoveRight;

    private void Start()
    {
        mFreezeMoveLeft = mFreezeMoveRight = false;
    }

    void Update ()
    {
        if (GameController.Instance.mCurrGameSatate == GameState.Ready)
        {
            Move();
            Shoot();
        }
    }

    private void Shoot()
    {
        if (mBullet == null)
        {
            mBullet = ((GameObject)GameObject.Instantiate(_BulletPref, _BulletNuzzle.transform.position, Quaternion.identity)).GetComponent<Bullet>();
            mBullet.transform.parent = this.transform;
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (mBullet != null && !mBullet.mIsFiring)
                {
                    mBullet.Fire();
                }
            }
        }
    }

    private void Move()
    {
        mInput = Input.GetAxis("Horizontal");
        if (mFreezeMoveLeft && mInput > 0)
        {
            mInput = 0;
        }
        if (mFreezeMoveRight && mInput < 0)
        {
            mInput = 0;
        }
        transform.Translate(Vector3.right * mInput * _Speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("BoundryLeft"))
        {
            mFreezeMoveLeft = true;
        }
        if (other.tag.Equals("BoundryRight"))
        {
            mFreezeMoveRight = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("BoundryLeft"))
        {
            mFreezeMoveLeft = false;
        }
        if (other.tag.Equals("BoundryRight"))
        {
            mFreezeMoveRight = false;
        }
    }

    public void OnObjectHit()
    {
        GameController.Instance.OnPlayerHit();
    }
}
