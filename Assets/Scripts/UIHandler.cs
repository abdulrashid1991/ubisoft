﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    [SerializeField]
    private GameObject _StartGameBtn;
    [SerializeField]
    private GameObject _RestartButton;
    [SerializeField]
    private GameObject _InitiatingGO;
    [SerializeField]private Text _Level;
    [SerializeField]private Text _Score;
    [SerializeField]private Text _Life;

    public void SetLevel(int level)
    {
        _Level.text = "Level : " + level.ToString();
    }

    public void SetScore(int score)
    {
        _Score.text = "Score : " + score.ToString();
    }

    public void SetLife(int life)
    {
        _Life.text = "Life : " + life.ToString();
    }

    private void Update()
    {
        switch (GameController.Instance.mCurrGameSatate)
        {
            case GameState.Menu:
                _StartGameBtn.SetActive(true);
                _RestartButton.SetActive(false);
                _InitiatingGO.SetActive(false);
                break;
            case GameState.Iniating:
                _StartGameBtn.SetActive(false);
                _InitiatingGO.SetActive(true);
                break;
            case GameState.End:
                _RestartButton.SetActive(true);
                break;
            case GameState.Ready:
                _InitiatingGO.SetActive(false);
                _RestartButton.SetActive(false);
                break;

        }
    }
}
