﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {

    public float _Speed;
    
	void Update ()
    {
        transform.Translate(Vector3.forward * _Speed * Time.deltaTime * -1);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("PlayerHome"))
        {
            var exp = GameObject.Instantiate(GameController.Instance._BombExplotionPrefab, this.transform.position, Quaternion.identity);
            Destroy(exp, 0.1f);
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            IOnHittable hitObj = collision.gameObject.GetComponent<IOnHittable>();
            if (hitObj != null)
            {
                hitObj.OnObjectHit();
            }
            var exp = GameObject.Instantiate(GameController.Instance._BombExplotionPrefab, this.transform.position, Quaternion.identity);
            Destroy(exp, 0.1f);
            Destroy(this.gameObject);
        }
    }
}
