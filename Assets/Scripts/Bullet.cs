﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    [HideInInspector]public bool mIsFiring = false;
    public float _Tourque;
    public GameObject _EngineParticle;
    private float mSpeed = 1;
    private void Update()
    {
        if (mIsFiring)
        {
            mSpeed += _Tourque;
            transform.Translate(Vector3.forward * mSpeed * Time.deltaTime);
        }
    }

    public void Fire()
    {
        mIsFiring = true;
        this.transform.parent = null;
        _EngineParticle.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("EnemyHome"))
        {
            var exp = GameObject.Instantiate(GameController.Instance._BuletExplotionPrefab, this.transform.position, Quaternion.identity);
            Destroy(exp, 0.1f);
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            IOnHittable hitObj = collision.gameObject.GetComponent<IOnHittable>();
            if (hitObj != null)
            {
                hitObj.OnObjectHit();
            }
            var exp = GameObject.Instantiate(GameController.Instance._BuletExplotionPrefab, this.transform.position, Quaternion.identity);
            Destroy(exp, 0.1f);
            Destroy(this.gameObject);
        }
    }
}
