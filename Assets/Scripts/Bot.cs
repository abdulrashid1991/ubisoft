﻿using UnityEngine;
using System.Collections;
using System;

public class Bot : MonoBehaviour , IOnHittable
{
    public int mBotId;
    public GameObject _BombPref;

    public EnemyHandler mEnemyGandlerRef;
    public void Kill()
    {
        Destroy(this.gameObject);
    }

    public void Shoot()
    {
        GameObject.Instantiate(_BombPref, this.transform.position, Quaternion.identity);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("BoundryLeft") || other.tag.Equals("BoundryRight"))
        {
            mEnemyGandlerRef.UpdateDirection();
        }
        if (other.tag.Equals("PlayerHome"))
        {
            GameController.Instance.mCurrGameSatate = GameState.End;
        }
    }

    public void OnObjectHit()
    {
        Kill();
        mEnemyGandlerRef.OnBotDestroy();
    }
}
